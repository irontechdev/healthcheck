package healthcheck

import (
	"fmt"
	"time"
)

// TimeoutError is the error returned when a Timeout-wrapped Check takes too long
type TimeoutError time.Duration

func (e TimeoutError) Error() string {
	return fmt.Sprintf("timed out after %s", time.Duration(e).String())
}

// Timeout returns whether this error is a timeout (always true for timeoutError)
func (e TimeoutError) Timeout() bool {
	return true
}

// Temporary returns whether this error is temporary (always true for timeoutError)
func (e TimeoutError) Temporary() bool {
	return true
}

// Timeout adds a timeout to a Check. If the underlying check takes longer than
// the timeout, it returns an error.
func Timeout(check Check, timeout time.Duration) Check {
	return func() error {
		c := make(chan error, 1)
		go func() { c <- check() }()
		select {
		case err := <-c:
			return err
		case <-time.After(timeout):
			return TimeoutError(timeout)
		}
	}
}
