package healthcheck

import (
	"errors"
	"net"
	"testing"
	"time"
)

func TestTimeout(t *testing.T) {
	tooSlow := Timeout(func() error {
		time.Sleep(10 * time.Millisecond)
		return nil
	}, 1*time.Millisecond)
	err := tooSlow()
	var timeoutError TimeoutError
	if !errors.As(err, &timeoutError) {
		t.Errorf("expected a TimeoutError, got %v", err)
	}
	var netErr net.Error
	if !errors.As(err, &netErr) || !netErr.Timeout() {
		t.Errorf("expected Timeout() to be true, got %v", err)
	}

	notTooSlow := Timeout(func() error {
		time.Sleep(1 * time.Millisecond)
		return nil
	}, 20*time.Millisecond)
	if err = notTooSlow(); err != nil {
		t.Errorf("expected success, got %v", err)
	}
}
