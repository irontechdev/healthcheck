package healthcheck

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
)

// MetricsHandler allows each check to be exposed as a Prometheus gauge metric.
type MetricsHandler struct {
	handler   *Handler
	registry  prometheus.Registerer
	namespace string
}

func NewMetricsHandler(registry prometheus.Registerer, namespace string) *MetricsHandler {
	return &MetricsHandler{
		handler:   NewHandler(),
		registry:  registry,
		namespace: namespace,
	}
}

func (m *MetricsHandler) AddLivenessCheck(name string, check Check) {
	m.handler.AddLivenessCheck(name, m.wrap(name, check))
}

func (m *MetricsHandler) AddReadinessCheck(name string, check Check) {
	m.handler.AddReadinessCheck(name, m.wrap(name, check))
}

func (m *MetricsHandler) LiveEndpoint(w http.ResponseWriter, r *http.Request) {
	m.handler.LiveEndpoint(w, r)
}

func (m *MetricsHandler) ReadyEndpoint(w http.ResponseWriter, r *http.Request) {
	m.handler.ReadyEndpoint(w, r)
}

func (m *MetricsHandler) wrap(name string, check Check) Check {
	m.registry.MustRegister(prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   m.namespace,
			Subsystem:   "healthcheck",
			Name:        "status",
			Help:        "current check status (0: success, 1: failure)",
			ConstLabels: prometheus.Labels{"check": name},
		},
		func() float64 {
			if check() == nil {
				return 0
			}
			return 1
		},
	))
	return check
}
