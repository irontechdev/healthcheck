package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"os"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	"gitlab.com/irontechdev/healthcheck"
)

const (
	upstreamAddr = "upstream.example.com:5432"
	upstreamHost = "upstream.example.com"
	upstreamURL  = "http://upstream-svc.example.com:8080/healthy"
)

type command func(ctx context.Context, w io.Writer) error

type healthHandler interface {
	LiveEndpoint(w http.ResponseWriter, r *http.Request)
	ReadyEndpoint(w http.ResponseWriter, r *http.Request)
}

var commands = map[string]command{
	"simple":   simple,
	"database": database,
	"advanced": advanced,
	"metrics":  metrics,
}

func simple(_ context.Context, w io.Writer) error {
	health := healthcheck.NewHandler()
	health.AddReadinessCheck("upstream-dep-dns",
		healthcheck.DNSResolveCheck(upstreamHost, 50*time.Millisecond))
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))

	mux := buildMux(health)
	_, err := fmt.Fprint(w, dumpRequest(mux, "GET", "/ready?full"))
	return err
}

func advanced(_ context.Context, w io.Writer) error {
	health := healthcheck.NewHandler()
	health.AddReadinessCheck("upstream-dep-tcp",
		healthcheck.Async(healthcheck.TCPDialCheck(upstreamAddr, 50*time.Millisecond), 10*time.Millisecond))
	health.AddReadinessCheck("upstream-dep-http", healthcheck.HTTPGetCheck(upstreamURL, 500*time.Millisecond))
	health.AddLivenessCheck("custom-check-with-timeout", healthcheck.Timeout(func() error {
		// Simulate some work that could take a long time
		time.Sleep(time.Millisecond * 100)
		return nil
	}, 50*time.Millisecond))

	mux := buildMux(health)
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Hello, world!"))
	})
	mux.HandleFunc("/healthz", health.ReadyEndpoint)

	// Sleep for just a moment to make sure our Async handler had a chance to run
	time.Sleep(500 * time.Millisecond)

	_, err := fmt.Fprint(w, dumpRequest(mux, "GET", "/healthz?full"))
	return err
}

func database(_ context.Context, w io.Writer) error {
	// Connect to a database/sql database
	var db *sql.DB
	db = connectToDatabase()

	// Create a Handler that we can use to register liveness and readiness checks.
	// Add a readiness check to we don't receive requests unless we can reach
	// the database with a ping in <1 second.
	health := healthcheck.NewHandler()
	health.AddReadinessCheck("database", healthcheck.SQLDatabasePingCheck(db, 1*time.Second))

	// Make a request to the readiness endpoint and print the response.
	mux := buildMux(health)
	_, _ = fmt.Fprint(w, dumpRequest(mux, "GET", "/ready?full=1"))

	return nil
}

func metrics(_ context.Context, w io.Writer) error {
	registry := prometheus.NewRegistry()
	hc := healthcheck.NewMetricsHandler(registry, "example")
	hc.AddReadinessCheck("failing-check", func() error {
		return fmt.Errorf("example failure")
	})
	hc.AddLivenessCheck("successful-check", func() error {
		return nil
	})

	mux := buildMux(hc)
	mux.Handle("/metrics", promhttp.HandlerFor(registry, promhttp.HandlerOpts{}))
	_, err := fmt.Fprint(w, dumpRequest(mux, "GET", "/metrics"))
	return err
}

func run(ctx context.Context, cmd string, w io.Writer) error {
	cmdFn := commands[cmd]
	if cmdFn == nil {
		flag.Usage()
		os.Exit(2)
	}

	return cmdFn(ctx, w)
}

func main() {
	flag.Usage = func() {
		_, _ = fmt.Fprintf(os.Stderr, `
Usage: hcheck <command> [command_option]

	Command can be one of: simple, database, advanced, metrics
`)
	}

	flag.Parse()
	nFlags := len(flag.Args())
	if nFlags < 1 || nFlags > 2 {
		flag.Usage()
		os.Exit(2)
	}

	cmd := flag.Arg(0)
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()
	if err := run(ctx, cmd, os.Stdout); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%s exited with error: %s", cmd, err.Error())
		os.Exit(1)
	}
}

func buildMux(hh healthHandler) *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/live", hh.LiveEndpoint)
	mux.HandleFunc("/ready", hh.ReadyEndpoint)
	return mux
}

func dumpRequest(handler http.Handler, method string, path string) string {
	req, err := http.NewRequest(method, path, nil)
	if err != nil {
		panic(err)
	}
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)
	dump, err := httputil.DumpResponse(rr.Result(), true)
	if err != nil {
		panic(err)
	}
	return strings.Replace(string(dump), "\r\n", "\n", -1)
}

func connectToDatabase() *sql.DB {
	db, _, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	return db
}
