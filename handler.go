package healthcheck

import (
	"encoding/json"
	"net/http"
	"sync"
)

// Check is a health/readiness check.
type Check func() error

type Handler struct {
	mu              sync.RWMutex
	livenessChecks  map[string]Check
	readinessChecks map[string]Check
}

// NewHandler creates a new basic Handler
func NewHandler() *Handler {
	return &Handler{
		livenessChecks:  make(map[string]Check),
		readinessChecks: make(map[string]Check),
	}

}

// AddLivenessCheck adds a check that indicates that this instance of the
// application should be destroyed or restarted. A failed liveness check
// indicates that this instance is unhealthy, not some upstream dependency.
// Every liveness check is also included as a readiness check.
func (h *Handler) AddLivenessCheck(name string, check Check) {
	h.mu.Lock()
	defer h.mu.Unlock()
	h.livenessChecks[name] = check
}

// AddReadinessCheck adds a check that indicates that this instance of the
// application is currently unable to serve requests because of an upstream
// or some transient failure. If a readiness check fails, this instance
// should no longer receiver requests, but should not be restarted or
// destroyed.
func (h *Handler) AddReadinessCheck(name string, check Check) {
	h.mu.Lock()
	defer h.mu.Unlock()
	h.readinessChecks[name] = check
}

// LiveEndpoint is the HTTP handler for just the /live endpoint, which is
// useful if you need to attach it into your own HTTP handler tree.
func (h *Handler) LiveEndpoint(w http.ResponseWriter, r *http.Request) {
	h.handle(w, r, h.livenessChecks)
}

// ReadyEndpoint is the HTTP handler for just the /ready endpoint, which is
// useful if you need to attach it into your own HTTP handler tree.
func (h *Handler) ReadyEndpoint(w http.ResponseWriter, r *http.Request) {
	h.handle(w, r, h.readinessChecks, h.livenessChecks)
}

func (h *Handler) handle(w http.ResponseWriter, r *http.Request, checks ...map[string]Check) {
	if r.Method != http.MethodGet {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	checkResults := make(map[string]string)
	status := http.StatusOK
	for _, c := range checks {
		h.collectChecks(c, checkResults, &status)
	}

	// write out the response code and content type header
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)

	// unless ?full is set, return an empty body.
	if !r.URL.Query().Has("full") {
		_, _ = w.Write([]byte("{}\n"))
		return
	}

	// otherwise, write the JSON body ignoring any encoding errors (which
	// shouldn't really be possible since we're encoding a map[string]string).
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "    ")
	_ = encoder.Encode(checkResults)
}

func (h *Handler) collectChecks(checks map[string]Check, resultsOut map[string]string, statusOut *int) {
	h.mu.RLock()
	defer h.mu.RUnlock()
	for name, check := range checks {
		if err := check(); err != nil {
			*statusOut = http.StatusServiceUnavailable
			resultsOut[name] = err.Error()
		} else {
			resultsOut[name] = "OK"
		}
	}
}
