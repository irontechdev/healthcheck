package healthcheck

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabasePingCheck(t *testing.T) {
	assert.Error(t, SQLDatabasePingCheck(nil, 1*time.Second)(), "nil DB should fail")

	db, _, err := sqlmock.New()
	assert.NoError(t, err)
	assert.NoError(t, SQLDatabasePingCheck(db, 1*time.Second)(), "ping should succeed")
}

func TestDNSResolveCheck(t *testing.T) {
	assert.NoError(t, DNSResolveCheck("google.com", 5*time.Second)())
	assert.Error(t, DNSResolveCheck("nonexistent.heptio.com", 5*time.Second)())
}

func TestGoroutineCountCheck(t *testing.T) {
	assert.NoError(t, GoroutineCountCheck(1000)())
	assert.Error(t, GoroutineCountCheck(0)())
}
