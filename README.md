# healthcheck

Simple package to perform *liveness* and *readiness* checks on an application.
